
-- SUMMARY --

dejurefilter is a input filter module for Drupal. It queries the dejure.org
service for linking law expressions in any filtered text field with entries from
the dejure.org database.

For a full description of the module, visit the project page:
  http://drupal.org/node/1985992
To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/1985992


-- REQUIREMENTS --

* None.


-- INSTALLATION --

* Install as usual, see
  http://drupal.org/documentation/install/modules-themes/modules-7

* Go to admin/config/content/formats,
  and choose a text format you want this filter activate on.
  Activate the dejure.org Filter in the "Enabled filters" section.
  Configure the filter in the "Filter settings" vertical tab.


-- CONFIGURATION --

Further information about all the settings you can find on the project page.


-- DEJURE.ORG API DOCUMENTATION --

There is no documentation page on dejure.org what parameters they have and what
are the correct values. But they have links to "modules" for other systems on
this page: http://dejure.org/vernetzung.html

They will also send you example code if you send a mail to info@dejure.org.

After reading through the example code and the Joomla module I was able to
identify the following parameters which are accepted by a POST request on this
page: http://rechtsnetz.dejure.org/dienste/vernetzung/vernetzen (There is also a
test form there you can use for testing.)

* 'Originaltext'
* 'Anbieterkennung'
* 'Dokumentkennung'
* 'Version'
* 'AktenzeichenIgnorieren'
* 'Zeilenwechsel'
* 'Tooltip'
* 'format_intern'
* 'class'
* 'target'
* 'format'

It is not quite clear to me what some of these parameters do. Or what values are
OK. Some seem to do the same thing. e.g.: "format" and "format_intern".

I asked for some additional documentation at dejure.org. And will update the
information here and on the project page if someone finds it useful for
improvements.
And yes - I will remove all the FIXME comments in the code if I got the
information from dejure what values are appropriate at these places.


-- CREDITS --

Author:
* Marcus Exner (marcusx) - http://drupal.org/user/837130
